import Home from '../pages/home/app'

interface route {
    path: string
    component: any
}

var routes: route[] = [{ path: '/', component: Home }]

export default routes
