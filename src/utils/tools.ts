function pxToRem(): void {
    ;(function(doc, win) {
        const docEl = doc.documentElement
        const resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize'
        const recalc = function() {
            const clientWidth = docEl.clientWidth
            if (!clientWidth) return
            if (clientWidth >= 640) {
                docEl.style.fontSize = '100px'
            } else {
                docEl.style.fontSize = 100 * (clientWidth / 640) + 'px'
            }
        }

        if (!doc.addEventListener) return
        win.addEventListener(resizeEvt, recalc, false)
        doc.addEventListener('DOMContentLoaded', recalc, false)
    })(document, window)
}

export default pxToRem
