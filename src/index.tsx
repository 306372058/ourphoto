import React from 'react'
import ReactDOM from 'react-dom'
import { HashRouter as Router, Route } from 'react-router-dom'
import routes from './router/'
// import './lib/flexible'
import { TransitionMotion, spring } from 'react-motion'

import './styles/reset.css'
import './styles/index.less'

const leavingSpringConfig = { stiffness: 60, damping: 15 }

class MainComponent extends React.Component<any, any> {
    constructor(props: any) {
        super(props)
        this.state = {
            mouse: [],
            now: 't' + 0,
        }
    }
    handleMouseMove({ pageX, pageY }: any) {
        // Make sure the state is queued and not batched.
        this.setState(() => {
            return {
                mouse: [pageX - 25, pageY - 25],
                now: 't' + Date.now(),
            }
        })
        return
    }

    handleTouchMove(e: any) {
        e.preventDefault()
        this.handleMouseMove(e.touches[0])
    }

    willLeave(styleCell: any) {
        return {
            ...styleCell.style,
            opacity: spring(0, leavingSpringConfig),
            scale: spring(2, leavingSpringConfig),
        }
    }
    render() {
        const {
            mouse: [mouseX, mouseY],
            now,
        } = this.state
        const styles =
            mouseX == null
                ? []
                : [
                      {
                          key: now,
                          style: {
                              opacity: spring(1),
                              scale: spring(0),
                              x: spring(mouseX),
                              y: spring(mouseY),
                          },
                      },
                  ]
        return (
            <TransitionMotion willLeave={this.willLeave} styles={styles}>
                {(circles: any) => (
                    <div className="container" onMouseMove={this.handleMouseMove.bind(this)} onTouchMove={this.handleTouchMove.bind(this)}>
                        {circles.map(({ key, style: { opacity, scale, x, y } }: any) => (
                            <div
                                key={key}
                                className="demo7-ball"
                                style={{
                                    opacity: opacity,
                                    scale: scale,
                                    transform: `translate3d(${x}px, ${y}px, 0) scale(${scale})`,
                                    WebkitTransform: `translate3d(${x}px, ${y}px, 0) scale(${scale})`,
                                }}
                            />
                        ))}
                        <Router>
                            <div>
                                {routes.map((element) => {
                                    return <Route key={element.path} exact path={element.path} component={element.component} />
                                })}
                            </div>
                        </Router>
                    </div>
                )}
            </TransitionMotion>
        )
    }
}

ReactDOM.render(<MainComponent />, document.getElementById('root'))
