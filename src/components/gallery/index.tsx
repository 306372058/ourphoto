import React from 'react'
import ReactGallery from 'react-photo-gallery'
import './index.css'

interface GalleryProps {
    photos: any[]
    openLightbox: any
}

class Gallery extends React.Component<GalleryProps, {}> {
    constructor(props: GalleryProps) {
        super(props)
    }
    render() {
        return (
            <div className="gallery">
                <ReactGallery photos={this.props.photos} onClick={this.props.openLightbox} />
            </div>
        )
    }
}

export default Gallery
