import React from 'react'
import './app.less'
import Carousel, { Modal, ModalGateway } from 'react-images'
import pxToRem from '../../utils/tools'
pxToRem()

interface HomeState {
    viewerIsOpen: boolean
    currentImage: number
    photos: any[]
}

const introPositions = [
    { tx: -0.6, ty: -0.3, s: 1.1, r: -20 },
    { tx: 0.2, ty: -0.7, s: 1.4, r: 1 },
    { tx: 0.5, ty: -0.5, s: 1.3, r: 15 },
    { tx: -0.2, ty: -0.4, s: 1.4, r: -17 },
    { tx: -0.15, ty: -0.4, s: 1.2, r: -5 },
    { tx: 0.7, ty: -0.2, s: 1.1, r: 15 },
]
const winsize = { width: window.innerWidth, height: window.innerHeight }

class Home extends React.Component<{}, HomeState> {
    constructor(props: any) {
        super(props)
        this.state = {
            viewerIsOpen: false,
            currentImage: 0,
            photos: [
                {
                    src: 'https://s2.ax1x.com/2019/08/26/mflN6J.png',
                },
                {
                    src: 'https://s2.ax1x.com/2019/08/26/mfidc8.png',
                },
            ],
        }
    }
    openLightbox(index: any) {
        this.setState({
            currentImage: index,
            viewerIsOpen: true,
        })
    }
    closeLightbox() {
        this.setState({
            viewerIsOpen: false,
        })
    }
    componentDidMount() {
        let gridItems = [].slice.call(document.querySelectorAll('.item'))
        gridItems.forEach((item: any, pos) => {
            var itemOffset = item.getBoundingClientRect()
            var settings = introPositions[pos]
            var enter = {
                x: winsize.width / 2 - (itemOffset.left + item.offsetWidth / 2),
                y: winsize.height - (itemOffset.top + item.offsetHeight / 2),
            }
        })
    }
    render() {
        return (
            <div className="photos">
                <ul className="gallery">
                    {this.state.photos.map((item, index) => {
                        return (
                            <li className="item" key={index} onClick={this.openLightbox.bind(this, index)}>
                                <img src={item.src} alt="" />
                            </li>
                        )
                    })}
                </ul>
                <ModalGateway>
                    {this.state.viewerIsOpen ? (
                        <Modal onClose={this.closeLightbox.bind(this)}>
                            <Carousel
                                currentIndex={this.state.currentImage}
                                views={this.state.photos.map((x) => ({
                                    ...x,
                                    srcset: x.src,
                                    caption: x.title,
                                }))}
                                onClose={function() {}}
                            />
                        </Modal>
                    ) : null}
                </ModalGateway>
            </div>
        )
    }
}

export default Home
